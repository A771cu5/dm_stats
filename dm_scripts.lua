
function startleading()
	SetVariable("leading", 1)
end

function stopleading()
	SetVariable("leading", 0)
end

function move(direction)
	local leading = tonumber(GetVariable('leading'))
	if leading == 1 then
		Send("lead " .. direction)
	else
		Send(direction)
	end
end

opposite_directions = {}
opposite_directions['n'] = 's'
opposite_directions['ne'] = 'sw'
opposite_directions['e'] = 'w'
opposite_directions['se'] = 'nw'
opposite_directions['s'] = 'n'
opposite_directions['sw'] = 'ne'
opposite_directions['w'] = 'e'
opposite_directions['nw'] = 'se'
opposite_directions['u'] = 'd'
opposite_directions['d'] = 'u'

function setkey(key)
	SetVariable('key', key)
end

function key(direction, barrier)
	Send('unlock ' .. direction .. ' ' .. barrier .. ' with ' .. GetVariable('key'))
	Send('open ' .. direction .. ' ' .. barrier)
	move(direction)
	Send('close ' .. opposite_directions[direction] .. ' ' .. barrier)
	Send('lock ' .. opposite_directions[direction] .. ' ' .. barrier .. ' with ' .. GetVariable('key'))
end

function improve_skill(skill_text)
	local original = skill_text
	local cleaned = string.gsub(original, "['-]", "_")
	cleaned = string.gsub(cleaned, " ", "_")
	local current = GetVariable(cleaned)
	if current == nil then
		current = 0
	else
		current = tonumber(current)
	end

	SetVariable(cleaned, current + 1)
end